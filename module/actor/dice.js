/**
 * A helper class for dice interactions
 */
export default class CVDice {

    static async RollTest({ speaker = null, flavor = null, title = null, actor = null, rollMode = null, } = {}) {

        let data = actor.data.data;
        let rolled = false;

        const messageOptions = { rollMode: rollMode || game.settings.get("core", "rollMode") };

        const template = 'systems/capitainevaudou/templates/actor/popup.html';

        let dialogData = {
            data: data,
            rollMode: messageOptions.rollMode,
            rollModes: CONFIG.Dice.rollModes,
        };

        let buttons = {
            ok: {
                label: "Roll",
                icon: '<i class="fas fa-dice"></i>',
                callback: (html) => {
                    roll = this._handleRoll({
                        form: html[0].querySelector("form"),
                        actor: actor,
                        data: data,
                        speaker: speaker,
                        flavor: flavor,
                    });
                    rolled = true;
                },
            },
            cancel: {
                icon: '<i class="fas fa-times"></i>',
                label: "Cancel",
            },
        };

        const html = await renderTemplate(template, dialogData);
        
        //Create Dialog window
        let roll;
        return new Promise((resolve) => {
            new Dialog({
                title: title,
                content: html,
                buttons: buttons,
                default: 'ok',
                close: () => {
                    resolve(rolled ? roll : null);
                },
            }).render(true);
        });
    }

    static _handleRoll({ form = null, actor = null, data = {}, speaker = null, title = '', flavor = '' }) {
        console.log("Roll!");

        // Roll the dice
        let roll = new Roll("2d6");
        roll.roll();

        // Get values
        let difficulty = form ? form.difficulty.value : null;
        let skill = form ? form.skill.value : null;
        let energy = form ? form.energy.value : null;

        // Set template data
        let templateData = {};
        templateData.diceResult = roll.results[0];

        templateData.dieResult = [];
        templateData.dieResult.push(roll.terms[0].results[0].result);
        templateData.dieResult.push(roll.terms[0].results[1].result);

        let rollMode = form ? form.rollMode.value : game.settings.get("core", "rollMode");
        templateData.title = "Title !";

        let rollModes = CONFIG.Dice.rollModes;
        templateData.flavor = game.i18n.localize(rollModes[rollMode]);  

        templateData.details = "Details?!"
        templateData.tags = [];
        templateData.tags.push("No tags on the wall plz");
        templateData.tags.push("I'm watching U!");

        let template = 'systems/capitainevaudou/templates/actor/dialog-roll.html';

        // Setup Chat Message
        let chatData = {
            user: game.user._id,
            rollMode: rollMode,
            speaker: ChatMessage.getSpeaker({
                actor: actor
            })
        };

        if (["gmroll", "blindroll"].includes(chatData.rollMode)) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
        
        if (chatData.rollMode === "blindroll") chatData["blind"] = true;
        else if (chatData.rollMode === "selfroll") chatData["whisper"] = [game.user];

        // Display roll message
        renderTemplate(template, templateData).then(content => {
            chatData.content = content;
            chatData.sound = CONFIG.sounds.dice;

            ChatMessage.create(chatData);
        });

    }

}