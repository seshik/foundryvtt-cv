import CVDice from "./dice.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class CapitaineVaudouActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

  }

  rollDice(actorData, options = { event: null }, tempSkill) {

    // Roll and return
    return CVDice.RollTest({
      data: this.data.data,
      speaker: ChatMessage.getSpeaker({ actor: this }),
      flavor: "test_flavor",
      title: "jet de dés",
      actor: this,
    });
  }

}