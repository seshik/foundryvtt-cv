/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class CapitaineVaudouActorSheet extends ActorSheet {

  /** @override */
  get actor() {
    return super.actor;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["capitainevaudou", "sheet", "actor"],
      template: "systems/capitainevaudou/templates/actor/actor-sheet.html",
      width: 800,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "attributes" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {

    const data = super.getData();
    const actorData = data.actor;

    // Prepare items.
    if (actorData.type == 'character') {
      this._prepareCharacterData();
      this._prepareDetailAttributes();
      this._prepareCharacterItems(data);
    }
    return data;
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData() {

    let constituentsList = duplicate(this.actor.data.data.constituents);
    for (let [key, constituents] of Object.entries(constituentsList)) {
      constituents.label = game.i18n.localize("actor-sheet.constituents-" + key);
    }

    let waysList = duplicate(this.actor.data.data.ways);
    for (let [key, ways] of Object.entries(waysList)) {
      ways.label = game.i18n.localize("actor-sheet.ways-" + key);
    }

    let targetList = duplicate(this.actor.data.data.target);
    for (let [key, target] of Object.entries(targetList)) {
      target.label = game.i18n.localize("actor-sheet.target-" + key);
    }

    let energyList = duplicate(this.actor.data.data.energy);
    for (let [key, energy] of Object.entries(energyList)) {
      energy.label = game.i18n.localize("actor-sheet.energy-" + key);
    }

    let statusList = duplicate(this.actor.data.data.status);
    for (let [key, status] of Object.entries(statusList)) {
      status.label = game.i18n.localize("actor-sheet.status-" + key);
    }

    let pirateSkills = duplicate(this.actor.data.data.attributes.basic);
    if (this.actor.data.data.sheetType == "basic") {
      if (pirateSkills.pirate_skills.pskill_1 == undefined) { // First init
        for (var i = 1; i <= 5; ++i) {
          pirateSkills.pirate_skills["pskill_" + i] = {
            name: game.i18n.localize("actor-sheet.attributes-basic-pskill_" + i),
            value: 1
          };

          if (i == 1) pirateSkills.pirate_skills["pskill_" + i].weaponName = "";
          else if (i == 5) pirateSkills.pirate_skills["pskill_" + i].language = "";

        }

        for (var i = 1; i <= 2; ++i) {
          pirateSkills.proficiency["proficiency_" + i] = {
            name: "",
            value: 1
          };
        }

        for (var i = 1; i <= 2; ++i) {
          pirateSkills.skill["skill_" + i] = "";
        }
      }
    }
    this.actor.update({
      "data.constituents": constituentsList,
      "data.ways": waysList,
      "data.target": targetList,
      "data.energy": energyList,
      "data.status": statusList,
      "data.attributes.basic": pirateSkills
    });

  }

  _prepareDetailAttributes() {

    let attrList = duplicate(this.actor.data.data.attributes.detail);

    if (attrList.length == 0) {
      let col = [];
      let attr = {};

      // Column X
      var times = 11;
      for (var i = 1; i <= times; i++) {
        attr = {
          label: game.i18n.localize("actor-sheet.attributes-detail.x." + i),
          value: "",
        }

        if (i == 6) attr.language = "";

        col.push(attr);
      }

      // Empty fields
      attr = {
        label: "",
        value: "",
      }
      col.push(attr);
      col.push(attr);

      attrList.push(col);

      // Column -4 
      col = [];
      times = 12;
      for (var i = 1; i <= times; i++) {
        attr = {
          label: game.i18n.localize("actor-sheet.attributes-detail.-4." + i),
          value: -4,
        }
        col.push(attr);
      }

      // Empty fields
      attr = {
        label: "",
        value: "",
      }
      col.push(attr);
      col.push(attr);

      attrList.push(col);

      // Column -2
      col = [];
      times = 8;
      for (var i = 1; i <= times; i++) {
        attr = {
          label: game.i18n.localize("actor-sheet.attributes-detail.-2." + i),
          value: -2,
        }

        if (i == 1) attr.weaponName = "";
        else if (i == 7) attr.language = "";

        col.push(attr);
      }

      // Empty fields
      attr = {
        label: "",
        value: "",
      }
      col.push(attr);
      col.push(attr);
      col.push(attr);
      col.push(attr);

      attrList.push(col);


      // Column 0
      col = [];
      times = 8;
      for (var i = 1; i <= times; i++) {
        attr = {
          label: game.i18n.localize("actor-sheet.attributes-detail.0." + i),
          value: 0,
        }
        col.push(attr);
      }

      // Empty fields
      attr = {
        label: "",
        value: "",
      }
      col.push(attr);
      col.push(attr);
      col.push(attr);
      col.push(attr);
      col.push(attr);
      col.push(attr);

      attrList.push(col);

      this.actor.update({ "data.attributes.detail": attrList });
    }

  }


  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // html.find('.header-fields select.sheetType').on('change', async (event) => {
    //   let sheetType = duplicate(this.actor.data.data.sheetType);
    //   this.actor.update({ "data.sheetType": sheetType });
    // });


    html.find('.constituents input.choiceConstituents').on('click', async (event) => {

      let constituentsData = duplicate(this.actor.data.data.constituents);
      let selVal = event.currentTarget.value;

      for (let [key, constituents] of Object.entries(constituentsData)) {
        if (key == selVal) constituents.isSelected = true;
        else constituents.isSelected = false;
      }

      this.actor.update({ "data.constituents": constituentsData });
    });

    html.find('.ways input.choiceWays').on('click', async (event) => {

      let waysData = duplicate(this.actor.data.data.ways);
      let selVal = event.currentTarget.value;

      for (let [key, ways] of Object.entries(waysData)) {
        if (key == selVal) ways.isSelected = true;
        else ways.isSelected = false;
      }

      this.actor.update({ "data.ways": waysData });

    });

    html.find('.targets input.choiceTargets').on('click', async (event) => {

      let targetsData = duplicate(this.actor.data.data.target);
      let selVal = event.currentTarget.value;

      for (let [key, targets] of Object.entries(targetsData)) {
        if (key == selVal) targets.isSelected = true;
        else targets.isSelected = false;
      }

      this.actor.update({ "data.target": targetsData });

    });

    html.find('.detailAttributesParam .attr-label input').on('change', async (event) => {

      const dataset = event.currentTarget.dataset;
      const selVal = event.currentTarget.value;

      let attrList = duplicate(this.actor.data.data.attributes.detail);

      attrList[dataset.key][dataset.keyline].label = selVal;

      await this.actor.update({ "data.attributes.detail": attrList });
    });

    html.find('.detailAttributesParam .attr-value input').on('change', async (event) => {

      const dataset = event.currentTarget.dataset;
      const selVal = event.currentTarget.value;

      let attrList = duplicate(this.actor.data.data.attributes.detail);

      attrList[dataset.key][dataset.keyline].value = selVal;

      await this.actor.update({ "data.attributes.detail": attrList });
    });

    html.find('.detailAttributesParam .attr-weapon-name input').on('change', async (event) => {

      const dataset = event.currentTarget.dataset;
      const selVal = event.currentTarget.value;

      let attrList = duplicate(this.actor.data.data.attributes.detail);

      attrList[dataset.key][dataset.keyline].weaponName = selVal;

      await this.actor.update({ "data.attributes.detail": attrList });
    });

    html.find('.detailAttributesParam .attr-language input').on('change', async (event) => {

      const dataset = event.currentTarget.dataset;
      const selVal = event.currentTarget.value;

      let attrList = duplicate(this.actor.data.data.attributes.detail);

      attrList[dataset.key][dataset.keyline].language = selVal;

      await this.actor.update({ "data.attributes.detail": attrList });
    });


    // Add Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    html.find('div.rollDices').on('click', (event) => {
      this.actor.rollDice();
    });
  }

  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    const object = [];
    const gad = [];
    const wanga = [];
    const weapon = [];
    const veve = [];

    for (let i of sheetData.items) {
      let item = i.data;

      switch (i.type) {
        case 'object':
          object.push(i);
          break;

        case 'gad':
          gad.push(i);
          break;

        case 'wanga':
          wanga.push(i);
          break;

        case 'weapon':
          weapon.push(i);
          break;

        case 'veve':
          veve.push(i);
          break;

      }
    }

    actorData.object = object;
    actorData.gad = gad;
    actorData.wanga = wanga;
    actorData.weapon = weapon;
    actorData.veve = veve;
  }

}
