/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class CapitaineVaudouShipSheet extends ActorSheet {

  /** @override */
  get actor() {
    return super.actor;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["capitainevaudou", "sheet", "actor"],
      template: "systems/capitainevaudou/templates/actor/ship-sheet.html",
      width: 800,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
  getData() {

    const data = super.getData();
    const actorData = data.actor;

    // Prepare ship
    if (actorData.type == 'ship') {
      this._prepareShip(actorData);
    }
    return data;

  }

  _prepareShip(actorData) {

    this.initShipList(actorData);

    this.initShipType(actorData);

    this.setSails(actorData);

    this.setHull(actorData);

    this.setCannons(actorData);

  }

  initShipType(actorData) {
    let shipType = duplicate(actorData.data.shipType);

    // By default, display the merchant ship
    if (shipType == "") this.setShipType("merchant")

  }

  initShipList(actorData) {

    let shipList = duplicate(actorData.data.shipList);

    if (shipList.length == 0) {

      let shipType = {
        type: "merchant",
        sails: 18,
        hull: 15,
        crew: {
          manoeuvre: 21,
          complement: 40,
          max: 45
        },
        draught: 4,
        performance: 90,
        perfManoeuvre: 4,
        cannons: {
          deck: 1,
          quantityByDeck: 4,
          bmr : [{value:0,length:4}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "corvette",
        sails: 32,
        hull: 24,
        crew: {
          manoeuvre: 40,
          complement: 80,
          max: 120
        },
        draught: 5,
        performance: 110,
        perfManoeuvre: 5,
        cannons: {
          deck: 1,
          quantityByDeck: 10,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:3},{value:3,length:1}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "frigate",
        sails: 36,
        hull: 24,
        crew: {
          manoeuvre: 50,
          complement: 140,
          max: 210
        },
        draught: 5,
        performance: 105,
        perfManoeuvre: 4,
        cannons: {
          deck: 2,
          quantityByDeck: 10,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:3},{value:3,length:1}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "galleon",
        sails: 24,
        hull: 30,
        crew: {
          manoeuvre: 25,
          complement: 55,
          max: 90
        },
        draught: 5,
        performance: 80,
        perfManoeuvre: 3,
        cannons: {
          deck: 1,
          quantityByDeck: 9,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:3}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "schooner",
        sails: 12,
        hull: 12,
        crew: {
          manoeuvre: 18,
          complement: 50,
          max: 90
        },
        draught: 3,
        performance: 120,
        perfManoeuvre: 6,
        cannons: {
          deck: 1,
          quantityByDeck: 8,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:2}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "sloop",
        sails: 9,
        hull: 9,
        crew: {
          manoeuvre: 8,
          complement: 35,
          max: 50
        },
        draught: 1,
        performance: 90,
        perfManoeuvre: 5,
        cannons: {
          deck: 1,
          quantityByDeck: 8,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:2}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "coastguard",
        sails: 9,
        hull: 9,
        crew: {
          manoeuvre: 8,
          complement: 35,
          max: 50
        },
        draught: 1,
        performance: 90,
        perfManoeuvre: 5,
        cannons: {
          deck: 1,
          quantityByDeck: 8,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:2}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "cutter",
        sails: 9,
        hull: 12,
        crew: {
          manoeuvre: 10,
          complement: 40,
          max: 60
        },
        draught: 2,
        performance: 100,
        perfManoeuvre: 6,
        cannons: {
          deck: 1,
          quantityByDeck: 8,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:2}]
        }
      }
      shipList.push(shipType);

      shipType = {
        type: "brigantine",
        sails: 12,
        hull: 9,
        crew: {
          manoeuvre: 14,
          complement: 30,
          max: 70
        },
        draught: 3,
        performance: 105,
        perfManoeuvre: 5,
        cannons: {
          deck: 1,
          quantityByDeck: 8,
          bmr : [ {value:0,length:3},{value:1,length:3},{value:2,length:2}]
        }
      }
      shipList.push(shipType);

      for (let [key, shipType] of Object.entries(shipList)) {
        shipType.label = game.i18n.localize("ship-sheet-type-" + shipType.type);
      }

      this.actor.update({ "data.shipList": shipList });

    }
  }

  setSails(actorData) {
    let sailsData = duplicate(actorData.data.sails);

    if (sailsData.damage.length == 0) {
      let sailsByLine = 0;

      // Calculate Sails values by line
      if ((sailsData.max % 5 == 0)) sailsByLine = 5;
      else {
        if ((sailsData.max % 4 == 0)) sailsByLine = 4;
        else sailsByLine = 3;
      }

      // Init Sails ranges
      let sailsDamage = sailsData.damage;

      let damage = {
        value: 0,
        nbLines: Math.ceil((sailsData.max / sailsByLine) / 3),
        lines: [],
      };
      sailsDamage.push(damage);

      damage = {
        value: -2,
        nbLines: Math.floor((sailsData.max / sailsByLine) / 3),
        lines: [],
      };
      sailsDamage.push(damage);

      damage = {
        value: -4,
        nbLines: Math.ceil((sailsData.max / sailsByLine) / 3),
        lines: [],
      };
      sailsDamage.push(damage);

      // Set values of the damage lines  
      let count = 1;
      let line = 0;
      let values = [];
      let value = {};

      for (let i = sailsData.max; i > 0; i--) {
        value = {
          point: i,
          state: false,
        };

        if (count < sailsByLine) {
          values.unshift(value);
        }
        else {
          values.unshift(value);
          sailsDamage[line].lines.push(values);

          // Next Damage range - Reset
          if (sailsDamage[line].lines.length == sailsDamage[line].nbLines) line++;
          count = 0;
          values = [];
        }

        count++;
      }

      this.actor.update({ "data.sails": sailsData });
    }
  }

  setHull(actorData) {
    let hullData = duplicate(actorData.data.hull);

    if (hullData.damage.length == 0) {
      let hullByLine = 0;

      // Calculate Hull values by line
      if ((hullData.max % 5 == 0)) hullByLine = 5;
      else {
        if ((hullData.max % 4 == 0)) hullByLine = 4;
        else hullByLine = 3;
      }

      // Init Hull ranges
      let hullDamage = hullData.damage = [];

      let damage = {
        value: 0,
        nbLines: (hullData.max / hullByLine) / 3,
        lines: [],
      };
      hullDamage.push(damage);

      damage = {
        value: -2,
        nbLines: (hullData.max / hullByLine) / 3,
        lines: [],
      };
      hullDamage.push(damage);

      damage = {
        value: -4,
        nbLines: (hullData.max / hullByLine) / 3,
        lines: [],
      };
      hullDamage.push(damage);

      // Set values of the damage lines  
      let count = 1;
      let line = 0;
      let values = [];
      let value = {};

      for (let i = hullData.max; i > 0; i--) {
        value = {
          point: i,
          state: false,
        };

        if (count < hullByLine) {
          values.unshift(value);
        }
        else {
          values.unshift(value);
          hullDamage[line].lines.push(values);

          // Next Damage range - Reset
          if (hullDamage[line].lines.length == hullDamage[line].nbLines) line++;
          count = 0;
          values = [];
        }

        count++;
      }
      this.actor.update({ "data.hull": hullData });
    }
  }

  setCannons(actorData) {
    let cannonsData = duplicate(actorData.data.cannons);

    // Port Side
    let portData = cannonsData.port;

    if (portData.cannons.length == 0) {
      let portdeckQty = portData.deckQty;
      let portQtyByDeck = portData.qtyByDeck;

      for (let i = 1; i <= portdeckQty; i++) {
        let deck = [];
        for (let j = portQtyByDeck; j > 0; j--) {
          let value = {
            point: j,
            state: true,
          };
          if (portData.value < j) value.state = false;
          deck.unshift(value);

        }
        portData.cannons.push(deck);
      }

      this.actor.update({ "data.cannons.port": portData });
    }

    // Starboard side
    let starboardData = cannonsData.starboard;

    if (starboardData.cannons.length == 0) {
      let starboarddeckQty = starboardData.deckQty;
      let starboardQtyByDeck = starboardData.qtyByDeck;

      for (let i = 1; i <= starboarddeckQty; i++) {
        let deck = [];

        for (let j = starboardQtyByDeck; j > 0; j--) {
          let value = {
            point: j,
            state: true,
          };
          if (starboardData.value < j) value.state = false;
          deck.unshift(value);
        }

        starboardData.cannons.push(deck);
      }
      this.actor.update({ "data.cannons.starboard": starboardData });

    }

  }

  setShipType(shipType) {

    let actorData = duplicate(this.actor.data.data);

    const newShipType = actorData.shipList.find(element => element.type == shipType);

    if (newShipType != undefined) {

      actorData.shipType = shipType;

      actorData.sails.max = newShipType.sails;
      actorData.sails.damage = [];

      actorData.hull.max = newShipType.hull;
      actorData.hull.damage = [];

      actorData.crew.manoeuvre.max = newShipType.crew.manoeuvre;
      actorData.crew.complement.max = newShipType.crew.complement;
      actorData.crew.max.max = newShipType.crew.max;


      actorData.draught = newShipType.draught;
      actorData.performance = newShipType.performance;
      actorData.perfManoeuvre = newShipType.perfManoeuvre;

      actorData.cannons.bmr = newShipType.cannons.bmr;
      actorData.cannons.port.deckQty = newShipType.cannons.deck;
      actorData.cannons.port.qtyByDeck = newShipType.cannons.quantityByDeck;
      actorData.cannons.port.cannons = [];

      actorData.cannons.starboard.deckQty = newShipType.cannons.deck;
      actorData.cannons.starboard.qtyByDeck = newShipType.cannons.quantityByDeck;
      actorData.cannons.starboard.cannons = [];

      this.actor.update({ "data": actorData });

    }

  }
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.header-fields select.shipType').on('change', async (event) => {
      this.setShipType(event.currentTarget.value)
    });

    html.find('.sheet-body input.sailsDamage').on('click', async (event) => {

      let sailsData = duplicate(this.actor.data.data.sails);
      let value = event.currentTarget.checked;

      let dataset = event.currentTarget.dataset;
      let damageId = dataset.damageId;
      let damageLine = dataset.damageLineId;
      let damageValueId = dataset.damageValueId;

      sailsData.damage[damageId].lines[damageLine][damageValueId].state = value;

      this.actor.update({ "data.sails": sailsData });
    });


    html.find('.sheet-body input.hullDamage').on('click', async (event) => {

      let hullData = duplicate(this.actor.data.data.hull);
      let value = event.currentTarget.checked;

      let dataset = event.currentTarget.dataset;
      let damageId = dataset.damageId;
      let damageLine = dataset.damageLineId;
      let damageValueId = dataset.damageValueId;

      hullData.damage[damageId].lines[damageLine][damageValueId].state = value;

      this.actor.update({ "data.hull": hullData });
    });

    html.find('.sheet-body input.cannons').on('click', async (event) => {

      let value = event.currentTarget.checked;

      let dataset = event.currentTarget.dataset;
      let side = dataset.side;

      let sideData;
      if (side == "port") sideData = duplicate(this.actor.data.data.cannons.port);
      else sideData = duplicate(this.actor.data.data.cannons.starboard);

      let key = dataset.deck;
      let keyLine = dataset.value;

      sideData.cannons[key][keyLine].state = value;

      if (side == "port") this.actor.update({ "data.cannons.port": sideData });
      else this.actor.update({ "data.cannons.starboard": sideData });

    });

    // Add Actor
    html.find('.crew-create').on('click', event => {
      let crewList = duplicate(this.actor.data.data.crew.list);

      let member = {
        rank: "",
        name: "",
      }

      crewList.push(member);
      this.actor.update({ "data.crew.list": crewList });

    });

    html.find('.crew-rank.value').on('change', event => {
      const idx = $(event.currentTarget).parents(".crew-value").data("itemId")  
      let crewList = duplicate(this.actor.data.data.crew.list);

      crewList[idx].rank = event.currentTarget.children[0].value
      this.actor.update({ "data.crew.list": crewList });
    });

    html.find('.crew-name.value').on('change', event => {
      const idx = $(event.currentTarget).parents(".crew-value").data("itemId")
      let crewList = duplicate(this.actor.data.data.crew.list);

      crewList[idx].name = event.currentTarget.children[0].value;
      this.actor.update({ "data.crew.list": crewList });
    });   

    // Delete Inventory Item
    html.find('.crew-delete').click(async (event) => {
      const idx = $(event.currentTarget).parents(".crew-value").data("itemId");

      let delCrewList = [];
      let actor = this.actor;
      let crewList = duplicate(this.actor.data.data.crew.list);
      delete crewList[idx];

      // Clear ranklist
      await this.actor.update({ "data.crew.list": delCrewList });

      // Set the new ranklist
      await this.actor.update({ "data.crew.list": crewList });
    });    

  }

}