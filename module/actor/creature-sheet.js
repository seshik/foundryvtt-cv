/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class CapitaineVaudouCreatureSheet extends ActorSheet {

  /** @override */
  get actor() {
    return super.actor;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["capitainevaudou", "sheet", "actor"],
      template: "systems/capitainevaudou/templates/actor/creature-sheet.html",
      width: 800,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {

    const data = super.getData();
    const actorData = data.actor;

    // Prepare items.
    if (actorData.type == 'npc') {
      this._prepareCharacterData(actorData);
      this._prepareCharacterItems(data);
    }
    return data;
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {

    let data = duplicate(actorData.data);

    for (let [key, energy] of Object.entries(data.energy)) {
      energy.label = game.i18n.localize("actor-sheet.energy-" + key);
    }

    for (let [key, status] of Object.entries(data.status)) {
      status.label = game.i18n.localize("actor-sheet.status-" + key);
    }

    this.actor.update({ "data": data });
  }


  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    html.find('.test.label a').on('click', (event) => {
      this.actor.rollDice();
    });
  }

  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    const gad = [];
    const wanga = [];
    const weapon = [];
    const veve = [];

    for (let i of sheetData.items) {
      let item = i.data;

      switch (i.type) {
        case 'gad':
          gad.push(i);
          break;

        case 'wanga':
          wanga.push(i);
          break;

        case 'weapon':
          weapon.push(i);
          break;

        case 'veve':
          veve.push(i);
          break;

      }
    }

    actorData.gad = gad;
    actorData.wanga = wanga;
    actorData.weapon = weapon;
    actorData.veve = veve;
  }

}