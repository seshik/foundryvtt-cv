// Import Modules
import { CapitaineVaudouActor } from "./actor/actor.js";
import { CapitaineVaudouActorSheet } from "./actor/actor-sheet.js";
import { CapitaineVaudouShipSheet } from "./actor/ship-sheet.js";
import { CapitaineVaudouNPCSheet } from "./actor/npc-sheet.js";
// import { CapitaineVaudouCreatureSheet } from "./actor/creature-sheet.js";

import { CapitaineVaudouItem } from "./item/item.js";
import { CapitaineVaudouItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function () {

  game.capitainevaudou = {
    CapitaineVaudouActor,
    CapitaineVaudouItem
  };

  // TURN ON OR OFF HOOK DEBUGGING
  CONFIG.debug.hooks = false;

  // Define custom Entity classes
  CONFIG.Actor.entityClass = CapitaineVaudouActor;
  CONFIG.Item.entityClass = CapitaineVaudouItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("capitainevaudou", CapitaineVaudouActorSheet, {
    types: ["character"],
    makeDefault: true
  });

  Actors.registerSheet("capitainevaudou", CapitaineVaudouShipSheet, {
    types: ["ship"],
    makeDefault: true
  });  
  Actors.registerSheet("capitainevaudou", CapitaineVaudouNPCSheet, {
    types: ["npc"],
    makeDefault: true
  });
  // Actors.registerSheet("capitainevaudou", CapitaineVaudouCreatureSheet, {
  //   types: ["npc"],
  //   makeDefault: false
  // });  


  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("capitainevaudou", CapitaineVaudouItemSheet, { makeDefault: true });

  Handlebars.registerHelper('cprTags', function (tagsInput) {
    let output = '<div class="tags">';
    tagsInput.forEach(element => {
      output += `<div class="tag">${element}</div>`;
    });
    output += '</div>';
    return output;
  });

  Handlebars.registerHelper("iff", function (a, operator, b, opts) {
    var bool = false;
    switch (operator) {
      case "==":
        bool = a == b;
        break;
      case ">":
        bool = a > b;
        break;
      case "<":
        bool = a < b;
        break;
      case "!=":
        bool = a != b;
        break;
      case "contains":
        if (a && b) {
          bool = a.includes(b);
        } else {
          bool = false;
        }
        break;
      default:
        throw "Unknown operator " + operator;
    }

    if (bool) return opts.fn(this);
    else return opts.inverse(this);
    
  });

  //Indicate selected option in select list
  Handlebars.registerHelper("select", function (value, options) {
    return options.fn(this)
      .split('\n')
      .map(function (v) {
        var t = 'value="' + value + '"'
        return !RegExp(t).test(v) ? v : v.replace(t, t + ' selected="selected"')
      })
      .join('\n')
  });

  Handlebars.registerHelper("switch", function(value, options) {
    this._switch_value_ = value;
    var html = options.fn(this);
    delete this._switch_value_;
    return html;
  });
  
  Handlebars.registerHelper("case", function(value, options) {
    if (value == this._switch_value_) {
      return options.fn(this);
    }
  });


});

Hooks.once("ready", async function () {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createCapitaineVaudouMacro(data, slot));
});